package com.webtable.practice;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTableToExcel {

@Test
	public void main() throws IOException {
		
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		driver.get("https://cosmocode.io/automation-practice-webtable/");
		
		String path=".\\datafiles\\TableListCountries1.xlsx";
		WebTable webtable=new  WebTable(path);
		
		/*//write headers in excel sheet
		 * It is not necessary to rewrite the names because when writing the data to the excel sheet it does
		webtable.setCellData("Sheet1", 0, 0, "Visited");
		webtable.setCellData("Sheet1", 0, 1, "Country");
		webtable.setCellData("Sheet1", 0, 2, "Capital(s)");
		webtable.setCellData("Sheet1", 0, 3, "Currency");
		webtable.setCellData("Sheet1", 0, 4, "Primary Language(s)");*/
		
		//capture table rows
		WebElement table=driver.findElement(By.xpath("//*[@id=\"countries\"]/tbody"));
		
		int rows=table.findElements(By.xpath("tr")).size();//rows present in web table
		
		for(int r=1;r<=rows;r++)
		{
			//read data from web table
		String visited=table.findElement(By.xpath("tr["+r+"]/td[1]")).getText();
		String country=table.findElement(By.xpath("tr["+r+"]/td[2]")).getText();
		String capital=table.findElement(By.xpath("tr["+r+"]/td[3]")).getText();
		String currency=table.findElement(By.xpath("tr["+r+"]/td[4]")).getText();
		String language=table.findElement(By.xpath("tr["+r+"]/td[5]")).getText();	
		
		System.out.println(visited+country+capital+currency+language);
		
		//writing the data in excel sheet
		webtable.setCellData("Sheet1", r, 0, visited);
		webtable.setCellData("Sheet1", r, 1, country);
		webtable.setCellData("Sheet1", r, 2, capital);
		webtable.setCellData("Sheet1", r, 3, currency);
		webtable.setCellData("Sheet1", r, 4, language);		
		
		
		}
		System.out.println("Web scrapping is done succesfully...");
		driver.close();
		
	}

}
