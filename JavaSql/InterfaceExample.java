/**
 * 
 */
package com.quality.testing.ExercisesQuestions;

/**
 * @author Karina
 *
 */
public interface InterfaceExample {
	 void learnCoding();
	 void learnProgrammingLanguage();
	 void exercise();
	}
abstract class Employee implements InterfaceExample {

 @Override public void learnCoding()
 {
     System.out.println("java and selenium");
 }
 @Override public void learnProgrammingLanguage()
 {
     System.out.println("Difference between abstract class and interface");
 }
}

class Manager extends Employee {
 @Override public void exercise()
 {
     System.out.println("This is an example");
 }
}
 class ClassMain {
 public static void main(String[] args)
 {  
     Manager Employee2 = new Manager();

     // Calls to the multiple functions
     Employee2.learnCoding();
     Employee2.learnProgrammingLanguage();
     Employee2.exercise();
 }
}
