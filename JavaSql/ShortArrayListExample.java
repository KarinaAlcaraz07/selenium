/**
 * 
 */
package com.quality.testing.ExercisesQuestions;

import java.util.ArrayList; 
import java.util.Collections;  
/**
 * @author Karina
 *
 */
public class ShortArrayListExample{
	
		 private ArrayList<String> arrayList;       

		 public ShortArrayListExample(ArrayList <String> arrayList) {
		    this.arrayList = arrayList;     
		  }       

		  public ArrayList<String> getArrayList() {         
		    return this.arrayList;     
		  }       

		  public ArrayList<String> sortAscending() {         
		    Collections.sort(this.arrayList);         
		    return this.arrayList;     
		  }       

		  public ArrayList<String> sortDescending() {         
		    Collections.sort(this.arrayList, Collections.reverseOrder());         
		    return this.arrayList;     
		  } 
		}