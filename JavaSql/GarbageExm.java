/**
 * 
 */
package com.quality.testing.ExercisesQuestions;

/**
 * @author Karina
 *
 */
public class GarbageExm {
			 
	 public static void main(String args[]){  
			  GarbageExm employee1=new GarbageExm();  
			  GarbageExm employee2=new GarbageExm();  
			  employee1=null;  
			  employee2=employee1;//now the first object referred is available for garbage collection  
			  
			  //Garbage collection is done by a thread called Garbage Collector(GC).
			  //This thread calls the finalize() method before object is garbage collected.
			  System.gc();  

	}
	 /*The finalize () method is invoked each time before the object is garbage collected. 
	  * This method can be used to perform cleanup processing.*/
	 public void finalize(){
		 //message to know if run the garbage collector
		 System.out.println("The object is garbage collected :)");
		 }  

}
