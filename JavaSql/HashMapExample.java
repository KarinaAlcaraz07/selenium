/**
 * 
 */
package com.quality.testing.ExercisesQuestions;

import java.util.HashMap;

import org.junit.Test;

/**
 * @author Karina
 *
 */
public class HashMapExample {

	@Test
	public void hashMap() {
		 // Creation of HashMap		
        HashMap<String, String> HashMapEx = new HashMap<>();
  
        //values to HashMap("keys", "values")
        HashMapEx.put("Language", "Java");
        HashMapEx.put("IDE", "Eclipse");
        HashMapEx.put("Excersise", "HashMap");
       
  
        System.out.println("This is the  '.isEmpty() method' ");
  
        // Checks whether the HashMap is empty or not
        if (!HashMapEx.isEmpty())
        {
            System.out.println("HashMap is not empty :)");
  
            //Access the contents of HashMap through Keys
            System.out.println("Test1 : " + HashMapEx.get("Language"));
            System.out.println("Test2 : " + HashMapEx.get("IDE"));
            System.out.println("Test3 : " + HashMapEx.get("Excersise"));
           
            //prints the size of HashMap using size method
            System.out.println("Size Of HashMap is: " + HashMapEx.size());
        }
    }
}
