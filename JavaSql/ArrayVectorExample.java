/**
 * 
 */
package com.quality.testing.ExercisesQuestions;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import org.testng.annotations.Test;

/** @author Karina
 */
public class ArrayVectorExample {
@Test
	public void arrayEx() {
		 // ArrayList
        ArrayList<String> array = new ArrayList<String>();
  
        //content arraylist
        array.add("A.element1");
        array.add("A.element2");
        array.add("A.element3");
  
        //traversing elements using Iterator'
        System.out.println("ArrayList elements are...");
        Iterator ite = array.iterator();
        while (ite.hasNext())
            System.out.println(ite.next());
  
        //Vector
        Vector<String> vctr = new Vector<String>();
        vctr.addElement("V.element1");
        vctr.addElement("V.element2");
        vctr.addElement("V.element3");
  
        // traversing elements using Enumeration
        System.out.println("Vector elements are...");
        Enumeration e = vctr.elements();
        while (e.hasMoreElements())
            System.out.println(e.nextElement());
    }
}