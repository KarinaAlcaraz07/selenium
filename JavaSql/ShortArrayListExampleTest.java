package com.quality.testing.ExercisesQuestions;
/**
 * @author Karina
 *
 */


import java.util.ArrayList;
import org.junit.Test;

public class ShortArrayListExampleTest {

	  @Test     
	  public void testSortAscendingDescending() throws Exception {         
	    ArrayList<String> LanguagesList = new ArrayList<>();         
	    LanguagesList.add("Java");         
	    LanguagesList.add("JavaScript");         
	    LanguagesList.add("Python");         
	    LanguagesList.add("Php");         
	    LanguagesList.add("Matlab");  
	    
	    ShortArrayListExample sortArrayList = new  ShortArrayListExample(LanguagesList);         
	    ArrayList<String> unsortedArrayList = sortArrayList.getArrayList();         
	    System.out.println("ArrayList: " + unsortedArrayList);         
	    ArrayList<String> sortedArrayListAscending = sortArrayList.sortAscending();         
	    System.out.println("Sorted ArrayList in Ascending Order : " + sortedArrayListAscending);         
	    ArrayList<String> sortedArrayListDescending = sortArrayList.sortDescending();         
	    System.out.println("Sorted ArrayList in Descending Order: " + sortedArrayListDescending);     
	  } 
	}