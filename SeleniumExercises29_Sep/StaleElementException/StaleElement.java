package com.practice.selm;

//import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


/* It's an item that is stale or no longer available, and this exception appears if we try to interact with the stale item.
 * is a WebDriver error that occurs because the referenced web element is no longer attached to the DOM.
 * For this example,i watch the video, https://www.youtube.com/watch?v=BrWUBwg-AWE */

public class StaleElement {
	public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	
	driver.get("https://live.skillrary.com/testing-app");
//	WebElement ele = driver.findElement(By.xpath("//a[.='FEEDBACK']"));
	//ele.click();
//	driver.findElement(By.name("downloadInvoice")).click();
	
	Pom p= new Pom(driver);
	p.feedbackbtn();
	p.downloadinvoice();
	driver.navigate().back();
	p.feedbackbtn();
	p.downloadinvoice();
	//ele.click();	
	
	/*
	WebElement elemt = driver.findElement(By.xpath("//a[.='FEEDBACK']"));
	elemt.click();
	driver.findElement(By.name("downloadInvoice")).click();
	*/
	
}
}


