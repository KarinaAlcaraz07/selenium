package com.practice.selm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class Pom {
	@FindBy (xpath="//a[.='FEEDBACK']")
	private WebElement feebackbtn;
	
	@FindBy(name="downloadInvoice")
	private WebElement downloadbtn;
	
	public Pom(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	public void feedbackbtn()
	{

		feebackbtn.click();
	}
	public void downloadinvoice()
	{
		downloadbtn.click();
	}

}
