package com.practice.selm;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;


/*When you have multiple iframes on your web page, driver.switchTo (). ParentFrame () should be used, 
 * it helps to switch control back to parent frame.
 When handling pop-up dialog windows within your web page,
 driver.switchTo (). DefaultContent () is used to change the control back to the default content in the window.
*To understand this topic, i whatch this video:https://www.youtube.com/watch?v=INdpVD26sDI */

public class SwitchWaitClose {
	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		
		
		/*The implicit wait is set for the entire lifetime of the webDriver object. 
		 *For example you want to wait a one time, 5 seconds for example before each element or many elements are loaded on the web page.
		 *so as not to rewrite the same code over and over again. implicit wait is used.*/
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		/*For example if a timeout is set, it will wait and go to the next step when the condition is true, 
		 * if not, it will throw an exception after waiting for the declared time.
		 * Explicit wait is applicable only once where specified.*/
		//driver.manage().timeouts().explicitlyWait(30, TimeUnit.SECONDS);
		
		
		driver.get("http://www.hyrtutorials.com/p/frames-practice.html");
		
		driver.findElement(By.id("name")).sendKeys("Frame Test");//mainwebpage
		Thread.sleep(3000);	
		driver.switchTo().frame(driver.findElement(By.id("frm3")));
		driver.switchTo().frame(driver.findElement(By.id("frm1")));
		
		
		Select courseDD= new Select(driver.findElement(By.id("course")));
		courseDD.selectByVisibleText("Python");
		Thread.sleep(3000);
		driver.switchTo().parentFrame();
		driver.findElement(By.id("name")).clear();
		driver.findElement(By.id("name")).sendKeys("frame3");//frame3
		Thread.sleep(3000);
		
		driver.switchTo().defaultContent();
		driver.findElement(By.id("name")).clear();
		driver.findElement(By.id("name")).sendKeys("Main webpage");
		Thread.sleep(3000);
		
		
		/* quit (): exits the controller, closing all associated windows that were open.
		 *  close (): closes the current window, closing the browser if it is the last window currently open*/
		driver.quit();
		//driver.close();
		
		}

}
