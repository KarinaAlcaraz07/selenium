package com.practice.selm;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;


//For this practice, watch this video that helped me understand drag and drop, "https://www.youtube.com/watch?v=vpS5nry__4I"
//The dragdrop; helps us to be able to drag and drop components from one position to another.
public class DragDrop {

	public static void main(String [] args) throws InterruptedException {		 
		//Test page url
		String url= "https://demoqa.com/droppable";		
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver.exe");
		 
		WebDriver driver=new ChromeDriver();
		driver.get(url);
		driver.manage().window().maximize();
		
		//Instantiate the actions class
		Actions action = new Actions(driver);
		
		//variables of type webelement
		//define the from and to (defino de donde y hacia donde movere el elemento)
		//I look for the id on the website
		WebElement from = driver.findElement(By.id("draggable"));
		WebElement to = driver.findElement(By.id("droppable"));
				
		//*******For drag and drop, it can be used in two ways:
		//First form: user the method drag and drop with the instantiated object.
		action.dragAndDrop(from, to).build().perform(); //from where and where will the action be
		
	    //the second way: is to use the x and y properties.
		//The horizontal axis is called the X-axis and the vertical axis is called the Y-axis.
		
		// action.dragAndDropBy(from, 270, 36).build().perform();
		
	
	}
}
