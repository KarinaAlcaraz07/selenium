package com.excel.practice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcelFile {
	/**
	 * @author Karina
	 *
	 */
	public String path;
	public FileInputStream fis = null;
	public FileOutputStream fileOut = null;


	public ReadExcelFile() {
			
	}
	//All Sheet
	public void readExcel(String filepath, String sheetName) throws IOException {
		File file = new File(filepath);
		FileInputStream inputStream = new FileInputStream(file);
			
		XSSFWorkbook newWorkbook = new XSSFWorkbook(inputStream);//where are all the data of the file that we load
		XSSFSheet newSheet = newWorkbook.getSheet(sheetName);
		
		int rowCount = newSheet.getLastRowNum() + newSheet.getFirstRowNum();
		
		for (int i=0; i <rowCount; i++) {
			XSSFRow row= newSheet.getRow(i);
			
			// number of cells that row has
			for (int j=0; j < row.getLastCellNum();j++) {
				System.out.println(row.getCell(j).getStringCellValue()+ "||");
				
			}
		}

	}
	//	read a specific value from a cell
	public String getCellValue(String filepath, String sheetName, int rowNumber, int cellNumer) throws IOException {
		
		
		File file = new File(filepath);//where will the file be
		FileInputStream inputStream = new FileInputStream(file);
			
		XSSFWorkbook newWorkbook = new XSSFWorkbook(inputStream);//where are all the data of the file that we load
		XSSFSheet newSheet = newWorkbook.getSheet(sheetName);
		
		XSSFRow row= newSheet.getRow(rowNumber);
		XSSFCell cell = row.getCell(cellNumer);
		
		
		
		return cell.getStringCellValue();
		
	}
			
	}


