package com.screenshot.practice;


import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class ScreenshotTest {
private WebDriver driver;

public String getDate() {
	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
	Date date = new Date();
	return dateFormat.format(date);
}

@Test
		public void setUp() throws Exception {
				System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver.exe");
				WebDriver driver = new ChromeDriver();
			
				driver.get("https://cosmocode.io/automation-practice-webtable/");
		        
		        
		       //Take the screenshot
		        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		        
		        //save screenshot, use try catch block to handle exception
		        /*First, the code in try is executed,
		        If there were no errors, catch is ignored, execution reaches the end of try and continues, skipping catch, but
		        if an error occurs, try execution stops and control flows to the beginning of catch.*/
		        try {
		            FileUtils.copyFile(screenshot, new File("screen_"+getDate()+".png"));
		        } catch (IOException e) {
		            System.out.println(e.getMessage());
		        }
		        
		        //close webdriver
		        driver.close();
		    }
		}
