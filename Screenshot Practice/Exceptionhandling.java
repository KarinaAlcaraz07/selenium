

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * 
 */

/**
 * @author Karina
 *
 */
public class Exceptionhandling {
private WebDriver driver;
	
	@Test	
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		//i use an implicit timeout to wait for the web to load
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("https://www.google.com");	
		
	
		/*
		 * It is used to search for an element, if it does not find it, it throws the exception
		try {
			driver.findElement(By.name("btnK")).click();
			System.out.println("the element be found");
		}
		catch(Exception e){
			System.out.println("the element not be found");
			
			
		}
		*/
		   
		try {
			//is used to change the control to an alert popup
				driver.switchTo().alert().accept();
				
				//is used when a user has attempted to access an alert when none is present.
			}catch (NoAlertPresentException E) {
				E.printStackTrace();
			
				try {
					//search for an item with a name
			driver.findElement(By.name("fake")).click();
			
			//Used to indicate that there are no more items
			}catch (NoSuchElementException  e) {
				//test print			
				System.out.println("Element is not found");
				System.out.println("Hello");
				//throw(e);
			}			
		}		
		
		/*open the pages and check that there are some elements on the page. 
		 * When item is absent,try and capture trap error messages and print to console notification.
		 * */
		    driver.findElement(By.name("q")).click();
		            Assert.assertTrue(isElementPresent(driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input"))));
		            driver.quit();
		        }
			//look for the element
		    public boolean isElementPresent(WebElement element) {
		        try {
		            element.isEnabled();
		            System.out.println("Element found! " + element);
		            return true;
		        } catch (org.openqa.selenium.NoSuchElementException e)  {
		            System.out.println("NoSuchElementException!");
		            return false;
		        }
		            
		       /* }
		        * 
		        * Is used when the item is not currently visible and therefore cannot be interacted with.
		        public boolean ElementNotVisibleException(WebElement element) {
			        try {
			        	driver.findElement(By.id("submit")).click();
			        	 System.out.println("ElementNotVisibleException!");
			        	return true;
			        } catch (org.openqa.selenium.NoSuchElementException e)  {
			            System.out.println("NoSuchElementException!");
			            return false;
			            */
			        }
		        
			//test print
		//System.out.println("Hello2");
			//driver.quit();	
}