package com.table.prac;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TableSelenium {
	
	public static void main(String[] args) throws InterruptedException {
		//open my browser
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		//the address is copied from the browser
		driver.get("file:///C:/Users/Karina/WEB/Table.html");
		
		//COLECTIONS
		//when dealing with multiple elements you should use findElements, not FindElement
		//cuando se trata de varios elementos es necesario usar findElements no FindElement
		
		//count the table columns
		List<WebElement> colsCount= driver.findElements(By.xpath("//table[@name =\"SampleTable\"]/tbody/tr/th"));
		//count the rows in the table
		List<WebElement> rowsCount= driver.findElements(By.xpath("//table[@name =\"SampleTable\"]/tbody/tr"));
		
		//print the number of each one
		System.out.println("No. of columns: " + colsCount.size() + " and " + " No. of rows: " + rowsCount.size());
		//print the column names
		System.out.println("Column names: ");
		
		for(WebElement col: colsCount) {
			System.out.println(col.getText());
			Thread.sleep(3000);
			
			
		
		}
		// wait time
	Thread.sleep(1000);
		
	driver.quit();
		
			}
	


}