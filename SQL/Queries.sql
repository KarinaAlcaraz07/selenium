use exercisedb;
CREATE TABLE Shop(
IdProduct int primary key not null,
NameProduct varchar(25),
CostProduct int
);
INSERT INTO Shop (IdProduct,NameProduct,CostProduct) VALUES
('01','Coke','10');
INSERT INTO Shop (IdProduct,NameProduct,CostProduct) VALUES
('02','Water','15');
INSERT INTO Shop (IdProduct,NameProduct,CostProduct) VALUES
('03','Juice','20');
INSERT INTO Shop (IdProduct,NameProduct,CostProduct) VALUES
('04','Beer','30');
INSERT INTO Shop (IdProduct,NameProduct,CostProduct) VALUES
('05','Wine','50');
INSERT INTO Shop (IdProduct,NameProduct,CostProduct) VALUES
('06','Coffe','15');
INSERT INTO Shop (IdProduct,NameProduct,CostProduct) VALUES
('07','Tea','12');
INSERT INTO Shop (IdProduct,NameProduct,CostProduct) VALUES
('08','Milk','25');
INSERT INTO Shop (IdProduct,NameProduct,CostProduct) VALUES
('09','yogurt','11');
INSERT INTO Shop (IdProduct,NameProduct,CostProduct) VALUES
('10','Jelly','30');
SELECT * FROM Shop;
SELECT * FROM Shop WHERE costProduct  LIMIT 3 ;
/*Query to get the 5 highest costs*/
SELECT  IdProduct,NameProduct, CostProduct FROM Shop WHERE costProduct ORDER BY costProduct DESC LIMIT 5;

SELECT * FROM Shop INNER JOIN Shop2 ON Shop.NameProduct = Shop2.NameProduct;
SELECT NameProduct FROM Shop INNER JOIN  Shop2 USING(NameProduct);

/*
SELECT NameProduct FROM  Shop
UNION
SELECT NameProduct FROM Shop2*/

/*SELECT NameProduct FROM  Shop
UNION ALL
SELECT NameProduct FROM Shop2*/



