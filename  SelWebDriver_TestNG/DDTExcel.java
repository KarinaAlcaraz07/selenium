/**
 * 
 */
package com.example.selenium;

import org.testng.annotations.Test;
import org.junit.After;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;

/**
 * @author Karina
 *
 */
public class DDTExcel {
	//read three users from an excel file, two are correct and one is not
	
ChromeDriver driver;
@Test(dataProvider = "testdata")

	public void DemoProject(String username, String password)throws InterruptedException {
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\Karina\\eclipse-works1\\SeleniumExcel\\src\\main\\resources\\chromedriver\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	driver.get("http://demo.guru99.com/test/newtours/login.php");
	
driver.findElement(By.name("userName")).sendKeys(username);
driver.findElement(By.name("password")).sendKeys(password);
driver.findElement(By.name("submit")).click();

Thread.sleep(3000);

Assert.assertTrue(driver.getTitle().matches("Login: Mercury Tours"), "Invalid credentials");
System.out.println("Login successful");
}

@After
 void ProgramTermination() {
	driver.close();
}

@DataProvider(name="testdata")
public Object[][] TestDataFeed(){
	
	ReadExcelFile config = new ReadExcelFile("C:\\Users\\Karina\\Desktop\\prueba.xlsx");
	int rows = config.getRowCount(0);
	
	Object[][] credentials = new Object[rows][2];
	
	for(int i=0;i<rows;i++) {
		credentials[i][0] = config.getData(0, i, 0);
		credentials[i][1]= config.getData(0,i,1);
		
	}	

	return credentials;	
	
	}

}
