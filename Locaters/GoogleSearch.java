package com.qualitiexercise.quiz;


	import org.junit.After;
	import org.junit.Before;
	import org.junit.Test;

	import static org.junit.Assert.assertEquals;

	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	
	

	public class GoogleSearch {
		
		private WebDriver driver;
		
		@Before
		public void setUp() {
		// declare the driver object of type chrome driver
			System.setProperty("webdriver.chrome.driver","./src/test/resources/chromedriver.exe");
			
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("https://www.google.com/");
			
		}
			// steps to run the test
		@Test
	
		// method to complete the elements of the page
		public void testGooglePage() {
			//locator
			//id
			WebElement searchbox = driver.findElement(By.name("q"));
			
			// clear any text		
			searchbox.clear();
			
			// what we want to search in google			
			searchbox.sendKeys("quality-exercise Locating elements in selenium");
			
			
			// to send the information
			searchbox.submit();
			
			// time we want it to wait			
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			
			// check the search results
			assertEquals("quality-exercise Locating elements in selenium - Google Search", driver.getTitle());
		}
		@After 
		public void tearDown() {
			// to close the browser once the test is done
			driver.quit();
		}
	}

