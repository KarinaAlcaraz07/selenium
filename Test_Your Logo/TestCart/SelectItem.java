import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;


import io.github.bonigarcia.wdm.WebDriverManager;

public class SelectItem {
	 public static void main(String[] args){
		 
	 WebDriverManager.chromedriver().setup();
	  WebDriver driver=new ChromeDriver();
	  String URL="http://automationpractice.com/index.php";
	  
	  // Open URL and Maximize browser window
	  driver.get(URL);
	  driver.manage().window().maximize();
	  driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);

	  //Click on Sign in
	  driver.findElement(By.linkText("Sign in")).click();
	  //Login
	  driver.findElement(By.id("email")).sendKeys("alcarazkarina11@gmail.com");
	  driver.findElement(By.id("passwd")).sendKeys("123Test$");
	  driver.findElement(By.id("SubmitLogin")).click();
	  //Click on the Women Category
	  driver.findElement(By.linkText("WOMEN")).click();
	  
	  //Xpath search of the last image of the dress (Busqueda de xpath de la ultima imagen del vestido)
	  WebElement LastImg=driver.findElement(By.xpath("//*[@id=\"center_column\"]/ul/li[7]/div/div[1]/div/a[1]/img"));
	 
	  //Find the xpath of the more button of that image(buscar el xpath del boton more de esa imagen)
	  WebElement MoreBtn=driver.findElement(By.xpath("//*[@id=\"center_column\"]/ul/li[7]/div/div[2]/div[2]/a[2]"));  
    
	  //Select the more button for that image(Seleccionar el boton de more para esa imagen)
	  Actions actions=new Actions(driver);
	  // click in the button
	  actions.moveToElement(LastImg).moveToElement(MoreBtn).click().perform();

	  //Select size as S(Seleccion de una talla)
	/*  WebElement Sizedrpdwn=driver.findElement(By.xpath("//*[@id='group_1']"));
	  Select oSelect=new Select(Sizedrpdwn);
	  oSelect.selectByVisibleText("S");*/

	  //Select the dress color
	  driver.findElement(By.name("Green")).click();

	  //Click on add to cart
	  driver.findElement(By.xpath("//p[@id='add_to_cart']//span[.='Add to cart']")).click();

	
}


}
