

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class TestItem {


	//objeto webdriver
	private WebDriver driver;
	//anotaciones de junit
	@Before
	public void setUp() {
		
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver.exe");
		driver = new ChromeDriver();
		//maximizar la ventana
		driver.manage().window().maximize();
		//url para abrir
		driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");	
		
	}
	@Test
	public void testLogin() {
		
			WebElement login = driver.findElement(By.name("SubmitLogin"));
			WebElement email = driver.findElement(By.cssSelector("[name='email']"));
			WebElement passwd = driver.findElement(By.id("passwd"));
			
				
			
				email.sendKeys("alcarazkarina11@gmail.com");
				passwd.sendKeys("123Test$");
								
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				
				login.click();
		        System.out.println("You Enter");
		        	        
		        
		        driver.findElement(By.linkText("WOMEN")).click();
		        System.out.println("Category Women");
    	        
		  	  
		  	  //Xpath search of the last image of the dress (Busqueda de xpath de la ultima imagen del vestido)
		  	  WebElement LastImg=driver.findElement(By.xpath("//*[@id=\"center_column\"]/ul/li[7]/div/div[1]/div/a[1]/img"));
		  	 
		  	  //Find the xpath of the more button of that image(buscar el xpath del boton more de esa imagen)
		  	  WebElement MoreBtn=driver.findElement(By.xpath("//*[@id=\"center_column\"]/ul/li[7]/div/div[2]/div[2]/a[2]"));  
		      
		  	  //Select the more button for that image(Seleccionar el boton de more para esa imagen)
		  	  Actions actions=new Actions(driver);
		  	  // click in the button
		  	  actions.moveToElement(LastImg).moveToElement(MoreBtn).click().perform();
		  	 System.out.println("More Selected");
 	        

		  	  //Select the dress color
		  	  driver.findElement(By.name("Green")).click();
		  	 System.out.println("Green color selected");
 	        

		  	  //Click on add to cart
		  	  driver.findElement(By.xpath("//p[@id='add_to_cart']//span[.='Add to cart']")).click();
		  	 System.out.println("Dress in your cart");
 	        
	}
	@After
	public void tearDown() {
		
		driver.quit();
	}

}
