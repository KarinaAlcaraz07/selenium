
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;



public class SearchPage {

 public static void main(String[] args) {
 //Open the URL
  WebDriverManager.chromedriver().setup();
  WebDriver driver=new ChromeDriver();
  String URL="http://automationpractice.com/index.php";
  driver.get(URL);
  //wait time (tiempo de espera)
  driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
  //max nav (maximizar la pesta�a)
  driver.manage().window().maximize();

  driver.findElement(By.linkText("Sign in")).click();//boton sing in
  
  //Enter email address(correo con el que se creara la cuenta)
  driver.findElement(By.cssSelector("[name='email_create']")).sendKeys("alcarazkarina11@gmail.com");
  
  //Account creation (Creacion de la cuenta)
  driver.findElement(By.xpath("//button[@name=\"SubmitCreate\"]")).click();
  driver.findElement(By.xpath("//input[@id=\"id_gender1\"]")).click();
  driver.findElement(By.name("customer_firstname")).sendKeys("Karina");
  driver.findElement(By.name("customer_lastname")).sendKeys("Leon");
  driver.findElement(By.id("passwd")).sendKeys("123Test$");
  
  //Enter your address (Direccion para el registro)
  driver.findElement(By.id("firstname")).sendKeys("Karina");
  driver.findElement(By.id("lastname")).sendKeys("Leon");
  driver.findElement(By.id("company")).sendKeys("Tata consultancy and services");
  driver.findElement(By.id("address1")).sendKeys("Queretaro");
  driver.findElement(By.id("city")).sendKeys("Queretaro");
    // Select State(Estado)
  WebElement statedropdown=driver.findElement(By.name("id_state"));
  Select oSelect=new Select(statedropdown);
  oSelect.selectByValue("13");
  driver.findElement(By.name("postcode")).sendKeys("58784");
  // Select Country (Pais, el unico permitido es USA)
  WebElement countrydropDown=driver.findElement(By.name("id_country"));
  Select oSelectC=new Select(countrydropDown);
  oSelectC.selectByVisibleText("United States");
  
  //number of telephone
  driver.findElement(By.id("phone_mobile")).sendKeys("4433906089");
  driver.findElement(By.xpath("//input[@name=\"alias\"]")).clear();
  driver.findElement(By.xpath("//input[@name=\"alias\"]")).sendKeys("My reference");
  driver.findElement(By.name("submitAccount")).click();
   }
}