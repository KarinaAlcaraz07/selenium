
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestUser {

	//objeto webdriver
	private WebDriver driver;
	//anotaciones de junit
	@Before
	public void setUp() {
		
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver.exe");
		driver = new ChromeDriver();
		//maximizar la ventana
		driver.manage().window().maximize();
		//url para abrir
		driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");	
		
	}
	@Test
	public void testLogin() {
		
			WebElement login = driver.findElement(By.name("SubmitLogin"));
			WebElement email = driver.findElement(By.cssSelector("[name='email']"));
			WebElement passwd = driver.findElement(By.id("passwd"));
			
				
			
				email.sendKeys("alcarazkarina11@gmail.com");
				passwd.sendKeys("123Test$");
								
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				
				login.click();
		        System.out.println("You Enter");

	}
	@After
	public void tearDown() {
		
		driver.quit();
	}

}
