import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SearchItems {
	public static void main(String[] args) throws InterruptedException{
		
		 //URL
		  WebDriverManager.chromedriver().setup();
		  WebDriver driver=new ChromeDriver();
		  String URL="http://automationpractice.com/index.php";
		  driver.get(URL);
		  driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
		  //max nav
		  driver.manage().window().maximize();

		  driver.findElement(By.linkText("Sign in")).click();//boton sing in
		  //valid username and password is entered		  
		  driver.findElement(By.cssSelector("[name='email']")).sendKeys("alcarazkarina11@gmail.com");	
		  driver.findElement(By.id("passwd")).sendKeys("123Test$"); 
		  driver.findElement(By.name("SubmitLogin")).click();
		
		  
		  // Initialise Actions class object
		  Actions actions=new Actions(driver);
		  //wait time
		  driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
		  
		  //selection category WOMEN
		   WebElement womenCateg=driver.findElement(By.linkText("WOMEN"));
		  
		  //ingreso en el cuadro de busqueda el producto que quiero buscar
		  //I enter the product I want to search for in the search box
		  WebElement BlousesBut=driver.findElement(By.xpath("//div[@id='block_top_menu']/ul/li[1]/ul/li[1]/ul//a[@title='Blouses']"));
		  actions.moveToElement(womenCateg).moveToElement(BlousesBut).click().perform();
		  Thread.sleep(2000);
		  
		  // Get the product name
		  String ProductName=driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[2]/ul[1]/li[1]/div[1]/div[2]/h5[1]/a[1]")).getText();
		  driver.findElement(By.id("search_query_top")).sendKeys(ProductName);
		  //click in the button search
		  driver.findElement(By.name("submit_search")).click(); 
		  

		 }

	}
